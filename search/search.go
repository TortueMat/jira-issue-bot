package search

import (
	log "github.com/sirupsen/logrus"

	"bytes"
	"encoding/json"
)

type Search struct {
	Jql       string   `json:"jql"`
	StartAt   int      `json:"startAt"`
	MaxResult int      `json:"maxResult"`
	Fields    []string `json:"fields"`
}

func NewDefaultSearch(jql string) Search {
	/*
	  Return a default search
	  parameter: <string> Jira Query Language request
	  return: <Search> A search struct
	*/
	return Search{
		Jql:       jql,
		StartAt:   0,
		MaxResult: 50,
		Fields: []string{
			"updated",
			"resolution",
		},
	}
}

func NewSearch(jql string, start int, max int, fields []string) Search {
	/*
	   Create a search from scratch
	   parameter: <string> Jira Query Language request
	   parameter: <int> Start issue index
	   parameter: <int> Max issue requested
	   parameter: <[]string> Array of fields
	   return: A new search
	*/
	return Search{
		Jql:       jql,
		StartAt:   start,
		MaxResult: max,
		Fields:    fields,
	}

}

func (search *Search) GetJson() []byte {
	/*
		Return JSON associated to Search struct
		return: <byte> A json

		i.e:
		```go
		s := NewDefaultSearch("updated < -5d")
		s.GetJson()
		{
			"jql": "updated < -5d",
			"startAt": 0,
			"maxResult": 50,
			"fields":[
				"updated",
				"resolution"
			]
		}
		```
	*/
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)

	err := enc.Encode(&search)
	if err != nil {
		log.Errorln(err)
		return nil
	}
	return []byte(buf.String())
}
