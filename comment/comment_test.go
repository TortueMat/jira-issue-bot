package comment

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreateAComment(t *testing.T) {
	c := NewComment("Hello, World", "role", "Administrator")
	assert.Equal(t, c.Body, "Hello, World")
	assert.Equal(t, c.Visibility.Type, "role")
	assert.Equal(t, c.Visibility.Value, "Administrator")
}

func TestCreateJsonFromStruct(t *testing.T) {
	c := NewComment("Hello, World", "role", "Administrator")
	expectedJson := []byte("{\"author\":{},\"body\":\"Hello, World\",\"updateAuthor\":{},\"visibility\":{\"type\":\"role\",\"value\":\"Administrator\"}}")
	assert.Equal(t, c.GetJson(), expectedJson)
}

func TestCreateCommentFromJson(t *testing.T) {
	data := []byte(`
		{
		    "self": "http://www.example.com/jira/rest/api/2/issue/10010/comment/10000",
		    "id": "10000",
		    "author": {
		        "self": "http://www.example.com/jira/rest/api/2/user?username=fred",
		        "name": "fred",
		        "displayName": "Fred F. User",
		        "active": false
		    },
		    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget venenatis elit. Duis eu justo eget augue iaculis fermentum. Sed semper quam laoreet nisi egestas at posuere augue semper.",
		    "updateAuthor": {
		        "self": "http://www.example.com/jira/rest/api/2/user?username=fred",
		        "name": "fred",
		        "displayName": "Fred F. User",
		        "active": false
		    },
		    "created": "2017-12-14T04:24:46.174+0000",
		    "updated": "2017-12-14T04:24:46.174+0000",
		    "visibility": {
		        "type": "role",
		        "value": "Administrators"
		    }
		}
	`)
	c := CreateCommentFromJson(data)
	assert.Equal(t, c.Author.DisplayName, "Fred F. User")
}

func TestCreateCommentFromTemplate(t *testing.T) {
	fileName := "comment_test.yaml"
	c := CreateCommentFromTemplate(fileName)
	assert.Equal(t, c.Visibility.Value, "Administrator")

}
