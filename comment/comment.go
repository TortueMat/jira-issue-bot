package comment

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
)

type Comment struct {
	Self         string     `json:"self,omitempty"`
	Id           string     `json:"id,omitempty"`
	Author       author     `json:"author,omitempty"`
	Body         string     `json:"body" yaml:"body"`
	UpdateAuthor author     `json:"updateAuthor,omitempty"`
	Created      string     `json:"created,omitempty"`
	Updated      string     `json:"updated,omitempty"`
	Visibility   visibility `json:"visibility" yaml:"visibility"`
}

type author struct {
	Self        string `json:"self,omitempty"`
	Name        string `json:"name,omitempty"`
	DisplayName string `json:"displayName,omitempty"`
	Active      bool   `json:"active,omitempty"`
}

type visibility struct {
	Type  string `json:"type" yaml:"type"`
	Value string `json:"value" yaml:"value"`
}

func NewComment(body string, visibilityType string, value string) Comment {
	/*
	  Create a new comment
	  parameter: <string> body is the main content of comment
	  parameter: <string> visibilityType is `group` or `role`
	  parameter: <string> value is name of `_type`
	  return: <Comment> A comment

	  i.e:
	  NewComment("Hello, World!", "role", "Administrator")
	  ```
	*/
	return Comment{
		Body: body,
		Visibility: visibility{
			Type:  visibilityType,
			Value: value,
		},
	}
}

func (comment *Comment) GetJson() []byte {
	/*
		Return JSON associated to Comment struct
		return: <byte> A json

		i.e:
		```go
		c := NewComment("Hello, World!", "role", "Administrator")
		c.GetJson()
		{
			"body": "Hello, World",
			"visibility": {
				"type": "role",
				"value": "Administrator"
			}
		}
		```
	*/
	jsonComment, err := json.Marshal(comment)
	if err != nil {
		log.Errorln(err)
		return nil
	}
	return jsonComment
}

func CreateCommentFromTemplate(fileName string) Comment {
	/*
		Return a comment directly created from a template
		parameter: <string> Template file name stored in templates/
		return: <Comment> A comment
	*/
	absFilePath, _ := filepath.Abs("templates/" + fileName)
	raw, err := ioutil.ReadFile(absFilePath)
	if err != nil {
		log.Errorln(err)
	}
	var c Comment
	yaml.Unmarshal(raw, &c)
	return c
}

func CreateCommentFromJson(data []byte) Comment {
	/*
		Return a comment directly created from a json response
		parameter: <[]byte> Json data received
		return: <Comment> A comment
	*/
	var c Comment
	json.Unmarshal(data, &c)
	return c
}
