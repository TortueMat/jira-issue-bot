package client

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/jira-issue-bot/comment"
	"gitlab.com/jira-issue-bot/issue"
	"gitlab.com/jira-issue-bot/search"

	"bytes"
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net/http"
	"path/filepath"
)

type HttpClient interface {
	Do(*http.Request) (*http.Response, error)
}

type JiraOpts struct {
	Host       string `yaml:"host"`
	Port       string `yaml:"port"`
	Username   string `yaml:"username"`
	Password   string `yaml:"password"`
	LastUpdate string `yaml:"last_update"`
}

type Jira struct {
	Options JiraOpts
	Client  HttpClient
}

type Response struct {
	Total  int           `json:"total"`
	Issues []issue.Issue `json:"issues"`
}

func NewJira(host string, port string, username string, password string, client HttpClient) *Jira {
	/*
	  Create a new Jira client
	  parameter: <string> Host is the Jira server domain name
	  parameter: <int> Port is the opened port on Jira server
	  return: <Jira> A Jira struct for getting and posting on a given endpoint
	*/
	return &Jira{
		Options: JiraOpts{
			Host:       host,
			Port:       port,
			Username:   username,
			Password:   password,
			LastUpdate: "0",
		},
		Client: client,
	}
}

func NewJiraFromConfFile(client HttpClient) Jira {
	/*
		Create a new Jira client from configuration file
		return: <Jira> A Jira struct for getting and posting on a given endpoint
	*/
	var jiraOpts JiraOpts
	absFilePath, _ := filepath.Abs("configuration/jira.yaml")
	raw, err := ioutil.ReadFile(absFilePath)
	if err != nil {
		log.Errorln(err)
	}
	yaml.Unmarshal(raw, &jiraOpts)
	return Jira{
		Options: jiraOpts,
		Client:  client,
	}
}

func (jira *Jira) PostComment(c comment.Comment, i issue.Issue) (*http.Response, error) {
	/*
	  Send a comment to Jira endpoint API /rest/api/2/issue/{issueIdOrKey}/comment
	  parameter: <comment> A comment to send
	  return: <http.Response, error> HTTP Response with body and status code, error
	*/
	req, _ := http.NewRequest("POST", jira.Options.Host+":"+jira.Options.Port+"/rest/api/2/issue/"+i.Id+"/comment", bytes.NewBuffer(c.GetJson()))
	req.SetBasicAuth(jira.Options.Username, jira.Options.Password)
	return jira.Client.Do(req)

}

func (jira *Jira) GetOldIssues(lastUpdate string) []issue.Issue {
	/*
		Get issues from JIRA that have not been updated since lastUpdate day
		JQL: `updated < -5d` to get issues that have not been updated since x last days
		parameter: <int> Last update of the issue (in day)
		return: <[]issue> An array of issues
	*/
	var _response Response
	jql := "jql=updated < " + lastUpdate + "d and resolution = Unresolved order by priority DESC,updated DESC"
	s := search.NewDefaultSearch(jql)
	req, _ := http.NewRequest("POST", jira.Options.Host+":"+jira.Options.Port+"/rest/api/2/search", bytes.NewBuffer(s.GetJson()))
	req.SetBasicAuth(jira.Options.Username, jira.Options.Password)
	response, err := jira.Client.Do(req)
	if err != nil {
		log.Errorln(err)
		return nil
	}
	body, err := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &_response)
	return _response.Issues
}

func (jira *Jira) DeleteIssue(i issue.Issue) bool {
	/* Delete a given issue
	parameter: <issue> Issue to delete
	return: <bool> Success of delete
	*/
	req, _ := http.NewRequest("DELETE", jira.Options.Host+":"+jira.Options.Port+"/rest/api/2/issue/"+i.Id, nil)
	req.SetBasicAuth(jira.Options.Username, jira.Options.Password)
	response, _ := jira.Client.Do(req)
	if response.StatusCode != 204 {
		log.Errorln("Error while deleting issue: ", i.Id, " Status code: ", response.StatusCode)
		return false
	}
	return true
}
