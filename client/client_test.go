package client

import (
	"gitlab.com/jira-issue-bot/comment"
	"gitlab.com/jira-issue-bot/issue"

	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

type MockHttpClient struct{}

func (m *MockHttpClient) Do(req *http.Request) (*http.Response, error) {
	if strings.Contains(req.URL.String(), "hello") {
		return &http.Response{
			StatusCode: 404,
		}, nil
	} else if strings.Contains(req.URL.String(), "/api/2/search") {
		return &http.Response{
			Body:       ioutil.NopCloser(bytes.NewBufferString(`{"total":1,"issues":[{"id":"5"},{"id":"12"}]}`)),
			StatusCode: 200,
		}, nil
	} else if req.Method == "DELETE" {
		return &http.Response{
			StatusCode: 204,
		}, nil
	} else {
		return &http.Response{
			StatusCode: 200,
		}, nil
	}
}

func TestCreateAClient(t *testing.T) {
	j := NewJira("localhost", "12800", "toto", "tata", nil)
	assert.Equal(t, j.Options.Host, "localhost")
	assert.Equal(t, j.Options.Port, "12800")
}

func TestPostAComment(t *testing.T) {
	j := NewJira("localhost", "12800", "toto", "tata", new(MockHttpClient))
	i := issue.NewIssue("XXX-XXX")
	c := comment.NewComment("Hello, World", "role", "Administrator")
	response, err := j.PostComment(c, i)
	assert.Equal(t, response.StatusCode, 200)
	assert.Equal(t, err, nil)
}

func TestGetOldIssues(t *testing.T) {
	j := NewJira("localhost", "12800", "toto", "tata", new(MockHttpClient))
	issues := j.GetOldIssues("5")
	assert.Equal(t, issues[1].Id, "12")
}

func TestDeleteAnIssue(t *testing.T) {
	j := NewJira("localhost", "12800", "toto", "tata", new(MockHttpClient))
	i := issue.NewIssue("XXX-XXX")
	assert.Equal(t, j.DeleteIssue(i), true)
}

func TestDeleteANonExistingIssue(t *testing.T) {
	j := NewJira("localhost", "12800", "toto", "tata", new(MockHttpClient))
	i := issue.NewIssue("hello")
	assert.Equal(t, j.DeleteIssue(i), false)
}

func TestNewJiraFromConfFile(t *testing.T) {
	j := NewJiraFromConfFile(nil)
	assert.Equal(t, j.Options.Host, "test.domain.com.name")
	assert.Equal(t, j.Options.Port, "80")
}
