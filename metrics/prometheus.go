package metrics

import (
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

func Counter(name string, docstring string, base_labels []string) *prometheus.CounterVec {
	/* Create a Prometheus Counter
	parameter: <string> Counter's name
	parameter: <string> Counter`s docstring
	parameter: <array of strings> Counter's labels
	return: A prometheus counter
	*/
	return prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: name,
		Help: docstring,
	}, base_labels)
}

func Histogram(name string, docstring string, base_labels []string) *prometheus.HistogramVec {
	/* Create a Prometheus Histogram
	parameter: <string> Histogram's name
	parameter: <string> Histogram`s docstring
	parameter: <array of strings> Histogram's labels
	return: A prometheus histogram
	*/
	return prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: name,
		Help: docstring,
	}, base_labels)
}

func PushMetrics(name string, metrics prometheus.Collector) bool {
	/* Send metrics to Prometheus Push Gateway
	paraneter: <string> Name of metrics job
	parameter: <interface> Metrics to send
	return: <bool> Return true if success
	*/
	err := push.Collectors(
		name,
		push.HostnameGroupingKey(),
		os.Getenv("METRICS_GATEWAY"),
		metrics,
	)
	if err != nil {
		log.Errorln("Could not push metrics to Pushgateway:", err)
		return false
	}
	return true
}
